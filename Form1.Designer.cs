namespace Demo_Schleifen_GUI
{
    partial class Demo_Schleifen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_while = new System.Windows.Forms.Button();
            this.cmd_while_do = new System.Windows.Forms.Button();
            this.cmd_for = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.lstBox = new System.Windows.Forms.ListBox();
            this.eventLog1 = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmd_while
            // 
            this.cmd_while.Location = new System.Drawing.Point(48, 40);
            this.cmd_while.Name = "cmd_while";
            this.cmd_while.Size = new System.Drawing.Size(108, 37);
            this.cmd_while.TabIndex = 0;
            this.cmd_while.TabStop = false;
            this.cmd_while.Text = "&While-Schleife";
            this.cmd_while.UseVisualStyleBackColor = true;
            this.cmd_while.Click += new System.EventHandler(this.cmd_while_Click);
            // 
            // cmd_while_do
            // 
            this.cmd_while_do.Location = new System.Drawing.Point(207, 40);
            this.cmd_while_do.Name = "cmd_while_do";
            this.cmd_while_do.Size = new System.Drawing.Size(124, 37);
            this.cmd_while_do.TabIndex = 1;
            this.cmd_while_do.TabStop = false;
            this.cmd_while_do.Text = "While-&Do-Schleife";
            this.cmd_while_do.UseVisualStyleBackColor = true;
            this.cmd_while_do.Click += new System.EventHandler(this.cmd_while_do_Click);
            // 
            // cmd_for
            // 
            this.cmd_for.Location = new System.Drawing.Point(378, 40);
            this.cmd_for.Name = "cmd_for";
            this.cmd_for.Size = new System.Drawing.Size(110, 37);
            this.cmd_for.TabIndex = 2;
            this.cmd_for.TabStop = false;
            this.cmd_for.Text = "&For-Schleife";
            this.cmd_for.UseVisualStyleBackColor = true;
            this.cmd_for.Click += new System.EventHandler(this.cmd_for_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(48, 252);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(86, 61);
            this.cmd_clear.TabIndex = 3;
            this.cmd_clear.TabStop = false;
            this.cmd_clear.Text = "&Anzeigen Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(300, 268);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(96, 29);
            this.cmd_end.TabIndex = 0;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // lstBox
            // 
            this.lstBox.ItemHeight = 16;
            this.lstBox.Location = new System.Drawing.Point(48, 111);
            this.lstBox.Name = "lstBox";
            this.lstBox.Size = new System.Drawing.Size(161, 132);
            this.lstBox.TabIndex = 0;
            this.lstBox.Visible = false;
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // Demo_Schleifen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 347);
            this.Controls.Add(this.lstBox);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_for);
            this.Controls.Add(this.cmd_while_do);
            this.Controls.Add(this.cmd_while);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Demo_Schleifen";
            this.Text = "Demo der veschiedenen Schleifen";
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmd_while;
        private System.Windows.Forms.Button cmd_while_do;
        private System.Windows.Forms.Button cmd_for;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.ListBox lstBox;
        private System.Diagnostics.EventLog eventLog1;
    }
}

