using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo_Schleifen_GUI
{
    public partial class Demo_Schleifen : Form
    {
        public Demo_Schleifen()
        {
            InitializeComponent();
            // Hilfe für die Buttons
            ToolTip tp = new ToolTip();
            tp.SetToolTip(cmd_clear,"Ausgabe lsöschen");
            tp.SetToolTip(cmd_end,"Programm löschen");
            tp.SetToolTip(cmd_while,"Ausführen der bedingten Schleife");
            tp.SetToolTip(cmd_while_do,"Ausführen der unbedingten Schleife");
            tp.SetToolTip(cmd_for,"Ausführen der For-Schleife");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            lstBox.Items.Clear();   // Listboxeinträge löschen
            lstBox.Visible = false; // Listbox nicht sichtbar
        }

        // Kopfgesteuerte Schleife
        private void cmd_while_Click(object sender, EventArgs e)
        {
            int zahl;                   // Variable setzen
            zahl = 1;                   // Startwert 1 zuweisen
            lstBox.Visible = true;      // Listbox sichtbar machen
            while (zahl <= 10)          // Verarbeitungsschleife; solange Zahl kleiner oder gleich 10 ist
                // Ausgabe des Textes un der Variable, anschließend Variable um eins erhöhen
                lstBox.Items.Add("Durchlauf " + zahl++);
        }

        // Fußgesteuerte Schleife
        private void cmd_while_do_Click(object sender, EventArgs e)
        {
            int zahl;
            zahl = 0;
            lstBox.Visible = true;
            do       // Begin der Schleife
            {
                // Schleifenrumpf
                lstBox.Items.Add("Durchlauf " + ++zahl);
            }
            while (zahl < 10);
        }

        // Zählschleife
        private void cmd_for_Click(object sender, EventArgs e)
        {
            lstBox.Visible = true;  // Listbox sichtbar machen
            // for(startwert;Laufbedingung;Steuerung)
            for (int zahl = 1; zahl <= 10; zahl++)
                lstBox.Items.Add("Durchlauf " + zahl);
        }
    }
}